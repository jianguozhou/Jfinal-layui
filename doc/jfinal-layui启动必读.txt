环境条件:jdk8,eclipse,maven，mysql

1、创建数据库：执行jfinal-layui.sql文件,
(如果用oracle数据库，则新建jfinal_layui用户，导入jfinal_layui.dmp文件)

2、修改config-dev.txt数据库配置参数

3、运行com.qinhailin.common.config.MainConfig.java的main方法即可，
     若端口占用问题，可修改undertow.txt相关参数
     
4、默认访问地址：http://localhost

5、管理员登录账号：admin  密码：123456

-------kkFileView文件在线预览服务-------------------------------------------------------------------------------
附件在线预览服务（file-online-preview）可从码云下载最新版发行包：https://gitee.com/kekingcn/file-online-preview
kkFileView 官网：https://kkfileview.keking.cn/
部署本地服务：
1、Windows系统解压kkFileView-4.1.0.rar文件，Linux系统解压kkFileView-4.1.0.tar.gz(进JFinal-layui交流群下载：970045838)
2、打开解压后文件夹的bin目录，运行startup脚本（Windows下以管理员身份运行startup.bat，Linux以root用户运行startup.sh）
3、浏览器访问本机8012端口 http://127.0.0.1:8012 即可看到项目演示用首页
4、将http://127.0.0.1:8012/onlinePreview配置到config-dev.txt文件的onlinePreviewUrl配置项中,并且onlinePreview=true开启在线预览服务

环境要求：
1、Java: 1.8+
2、OpenOffice或LiberOffice(Windows下已内置，CentOS或Ubuntu下会自动下载安装，MacOS下需要自行安装)
-------------------------------------------------------------------------------------------------

打包部署到服务器：
1：eclipse 打包：run as -->maven clean and package
（或者进入项目根目录用maven命令：mvn clean package）

2：打包完后，进入 JFinal-layu/target/JFinal-layui-release/JFinal-layui 目录
     
3：windows ：cmd运行命令： jfinal.bat start启动项目
   linux ： jfinal.sh start 启动项目， 
            jfinal.sh stop 关闭项目 ,
            jfinal.sh restart 重启项目
      
4、部署： JFinal-layui/target 目录下面还会有一个 JFinal-layui-release.zip 文件
	只需要把JFinal-layui-release.zip拷贝到服务器，然后解压,安装打包部署步骤配置运行即可。
	或者是直接复制JFinal-layui/target/JFinal-layu-release/JFinal-layui 目录下的文件部署


