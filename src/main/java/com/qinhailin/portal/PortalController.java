/**
 * Copyright 2019-2023 覃海林(qinhaisenlin@163.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 

package com.qinhailin.portal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.aop.Clear;
import com.jfinal.core.JFinal;
import com.jfinal.core.NotAction;
import com.jfinal.core.Path;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.Ret;
import com.jfinal.render.JsonRender;
import com.jfinal.upload.UploadFile;
import com.qinhailin.common.model.FileUploaded;
import com.qinhailin.common.base.BaseController;
import com.qinhailin.common.config.WebContant;
import com.qinhailin.common.intercepor.SessionInterceptor;

/**
 * 附件、公共方法类
 * 特别说明:该类的的viewPath="/"
 * @author QinHaiLin
 *
 */
@Path(value="/portal",viewPath="/")
public class PortalController extends BaseController {

	/**
	 * 
	 * go后面带视图目录参数 如：/portal/go/common/upload/upload，<br/>
	 * 将会访问/common/upload/upload.html文件
	 * 
	 * @author QinHaiLin
	 * @date 2018年8月2日
	 */
	public void go() {
		render(getAttr("view") + ".html");
	}

	/**
	 * 独立上传页面
	 * 
	 * @author QinHaiLin
	 * @date 2018年8月13日
	 */
	public void toUpload() {
		String objectId = getPara(0);
		if (objectId == null) {
			objectId = getVisitor().getCode();
		}
		setAttr("objectId", objectId);
		List<FileUploaded> fileList=getFileUploadListByObjectId(objectId);
		setAttr("fileList", fileList);
		setAttr("finished",fileList.size());
		render("common/upload/upload.html");
	}

	/**
	 * 用于嵌套在表单中上传<br/>
	 */
	public void toFormUpload() {
		String objectId = getPara(0);
		if (objectId == null) {
			objectId = getVisitor().getCode();
		}
		List<FileUploaded> fileList=getFileUploadListByObjectId(objectId);
		setAttr("objectId", objectId);
		setAttr("autoUpload",getPara(1));
		setAttr("showDelBtn",getPara(2));
		setAttr("accept",getPara(3));
		setAttr("multiple",getPara(4));
		setAttr("fileList", fileList);
		setAttr("finished",fileList.size());
		render("common/upload/formUpload.html");
	}
	
	/**
	 * 上传文件,可多附件上传
	 * 
	 * @author QinHaiLin
	 * @date 2018年8月1日
	 */
	public void upload() {
		List<UploadFile> uploadList = getFiles();
		String objectId = getPara() == null ? getPara("objectId") : getPara();
		List<String> url = saveFiles(uploadList, objectId);	
		UploadFile uf=uploadList.get(0);
		String fileName=uf.getFileName();
		Ret ret=Ret.ok("url", url).set("fileName", fileName);
		String fileType=fileName.substring(fileName.lastIndexOf(".")+1);
		ret.set("fileType", fileType.toLowerCase());
		renderJson(ret);	
	}

	/**
	 * 下载文件
	 * 
	 * @params url格式：18080615/18080615025800002
	 * @author QinHaiLin
	 * @date 2018年8月1日
	 */
	@Clear(SessionInterceptor.class)
	public void download() {
		String url = getAttr("url", "");
		FileUploaded fu;
		if (url.contains("/")) {
			fu = getFileUploaded(url);
		} else {
			fu = getFileUploadedByObjectId(url);
		}
		
		//实现服务器资源共享
		String fileTypeName = fu.getFileName().substring(fu.getFileName().lastIndexOf("."));
		//linux绝对路径下不转换路径
		if(!fu.getSavePath().startsWith("/")){
			fu.setSavePath(PathKit.getWebRootPath() +  "/" +
					WebContant.baseUploadPath + "/" + fu.getUrl() + fileTypeName);			
		}
		
		renderFile(new File(fu.getSavePath()), fu.getFileName());
	}

	/**
	 * 删除文件
	 * 
	 * @params url格式：18080615/18080615025800002
	 * @author QinHaiLin
	 * @date 2018年8月1日
	 */
	public void delete() {
		String url = getAttr("url", "");
		if (url.contains("/")) {
			deleteFileByUrl(url);
		} else {
			deleteFileByObjectId(url);
		}
		renderJson(Ret.ok());
	}

	/**
	 * 显示上传文件列表
	 * 
	 * @author QinHaiLin
	 * @date 2018年8月13日
	 */
	public void getFileList() {
		String objectId = getPara(0,getUserCode());
		setAttr("fileList", getFileUploadListByObjectId(objectId));
		setAttr("objectId", getPara());
		render("common/upload/fileList.html");
	}

	/**
	 * 下载模板文件
	 * 
	 * @params
	 * @author QinHaiLin
	 * @date 2018年8月2日
	 */
	@Clear(SessionInterceptor.class)
	public void temp() {
		renderFile(new File(PathKit.getWebRootPath() + "/" + WebContant.baseDownloadPath + getAttr("url")));
	}

	private static Map<String, String> imageContentType = new HashMap<String, String>();
	static {
		imageContentType.put("jpg","image/jpeg");
		imageContentType.put("jpeg","image/jpeg");
		imageContentType.put("png","image/png");
		imageContentType.put("tif","image/tiff");
		imageContentType.put("tiff","image/tiff");
		imageContentType.put("ico","image/x-icon");
		imageContentType.put("bmp","image/bmp");
		imageContentType.put("gif","image/gif");
	}
	
	/**
	 * 图片、pdf、txt预览，其他文件则下载
	 * 
	 * @params url格式：18080615/18080615025800002
	 * @author QinHaiLin
	 * @date 2019年6月16日
	 */
	@Clear(SessionInterceptor.class)
	public void view() {
		String url = getAttr("url", "");
		FileUploaded fu;
		if (url.contains("/")) {
			fu = getFileUploaded(getAttr("url"));
		} else {
			fu = getFileUploadedByObjectId(getAttr("url"));
		}
		
		if(fu==null){
			renderHtml("<img src=\"/static/img/error/error.png\">");
			return;
		}
		
		try {
			String fileTypeName = fu.getFileName().substring(fu.getFileName().lastIndexOf("."));
			fu.setSavePath(PathKit.getWebRootPath() +  "/" +
					WebContant.baseUploadPath + "/" + fu.getUrl() + fileTypeName);
			
			File image = new File(fu.getSavePath());
			@SuppressWarnings("resource")
			FileInputStream inputStream = new FileInputStream(image);
			int length = inputStream.available();
			byte data[] = new byte[length];
			getResponse().setContentLength(length);
			String fileName = image.getName();
			String fileType = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			getResponse().setContentType(imageContentType.get(fileType));
			inputStream.read(data);
			OutputStream toClient = getResponse().getOutputStream();
			if("txt".equals(fileType)){
				String txt=readTxt(fu.getSavePath());
				toClient.write(txt.getBytes());
			}else if(".jpg .png .ico .gif .jpeg .pdf".indexOf(fileType)!=-1){			
				toClient.write(data);
			}else{
				//不能预览的文件类型，直接下载
				renderFile(new File(fu.getSavePath()), fu.getFileName());
				return;
			}
			toClient.flush();
			toClient.close();
		} catch (IOException e) {
			handerException(e);
			renderHtml("<img src=\"/static/img/error/error.png\">");
			return;
		}
		renderNull();
	}
	
	/**
	 * 读取txt文件内容
	 * @param filePath
	 * @return
	 */
	@NotAction
	public String readTxt(String filePath) {		  
		StringBuffer txt = new StringBuffer();
		  try {
		    File file = new File(filePath);
		    if(file.isFile() && file.exists()) {
		      InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "GBK");
		      BufferedReader br = new BufferedReader(isr);
		      String lineTxt = null;
		      while ((lineTxt = br.readLine()) != null) {
		        txt.append(lineTxt).append("\r\n");
		      }
		      br.close();
		    } else {
		      System.out.println("文件不存在!");
		    }
		  } catch (Exception e) {
		    System.out.println("文件读取错误!");
		  }
		  
		  return txt.toString();
	}
	
	/**
	 * ueditor上传接口
	 * @author QinHaiLin
	 * @date 2019年11月17日
	 */
	@Clear(SessionInterceptor.class)
	public void ueditor(){
		if ("config".equals(getPara("action"))) {
			render("/static/libs/ueditor/jsp/config.json");
			return;
		}
		/**
		 * 对应 config.json 配置的 imageActionName: "uploadimage"
		 */
		if ( ! "uploadimage".equals(getPara("action"))) {
			renderJson("state", "UploadController 只支持图片类型的文件上传");
			return ;
		}
		
		UploadFile uploadFile = getFile();
		String url=saveFile(uploadFile, getVisitor().getCode());
		
		Ret ret=Ret.by("state", "SUCCESS")
		.set("url", JFinal.me().getContextPath()+"/portal/download/"+url)
		.set("title", uploadFile.getFileName())
		.set("original", uploadFile.getOriginalFileName())
		.set("type", uploadFile.getContentType())
		.set("size", uploadFile.getFile().length());
		
		render(new JsonRender(ret).forIE());	// 防止 IE 下出现文件下载现象
	}
	
}
