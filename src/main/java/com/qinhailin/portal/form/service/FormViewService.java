/**
 * Copyright 2019-2023 覃海林(qinhaisenlin@163.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 

package com.qinhailin.portal.form.service;

import java.util.Date;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Record;
import com.qinhailin.common.base.service.DbService;
import com.qinhailin.common.kit.IdKit;
import com.qinhailin.common.vo.Grid;

/**
 * 在线表单
 * @author QinHaiLin
 * @date 2019年4月24日  
 */
public class FormViewService extends DbService{
		
	/* (non-Javadoc)
	 * @see com.qinhailin.common.base.service.DbService#getTable()
	 */
	@Override
	public String getTable() {
		return "form_view";
	}

	/* (non-Javadoc)
	 * @see com.qinhailin.common.base.service.DbService#getPrimaryKey()
	 */
	@Override
	public String getPrimaryKey() {
		return "id";
	}
	
	public Grid queryPage(int pageNumber,int pageSize,Kv columns){
		Kv kv=new Kv();
		kv.set("name like", columns.get("name"));
		kv.set("code like",columns.get("code"));
		kv.set("status =", columns.get("status"));
		kv.set("tree_id =", columns.get("tree_id"));
		return page(pageNumber, pageSize, kv,"order by create_time desc");
	}
	
	public boolean createCodeTemplete(Record record) {
		String objectName=record.get("objectName");
		String tableComment=record.get("tableComment");
		Record entity=new Record();
		String[] html= {"添加页","修改页","列表页"};
		String[] code= {"add","edit","index"};
		boolean b=false;
		for(int i=0;i<html.length;i++) {
			entity.set("id", IdKit.createUUID());
			entity.set("tree_id", record.get("treeId"));
			entity.set("name", tableComment+html[i]);
			String c=objectName+"_"+code[i];
			Record rd=this.findPk("code", c);
			if(rd!=null)
				c+=2;
			entity.set("code", c);
			entity.set("status", "INIT");
			entity.set("template_view", record.get(code[i]+"Html"));
			entity.set("create_time", new Date());
			entity.set("descp", tableComment+html[i]);
			b=this.save(entity);
		}
		return b;
	}
}
